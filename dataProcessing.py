import numpy as np
import random
import pickle
from collections import Counter

hm_lines = 100000

def sample_handling(sample):
    row_count = 1
    featureset = []

    with open(sample, 'r') as f:
        contents = f.readlines()
        for l in contents[:hm_lines]:
            items = l.strip().split(",")
            if row_count == 1:
                ## Handle item names
                #print items
                pass
            else:
                features = items[:-1]
                featureset.append([features, items[-1]])
            row_count += 1
    return featureset

def create_feature_sets_and_labels(data, test_size = 0.1):
    features = []
    features += sample_handling(data)
    random.shuffle(features)
    features = np.array(features)

    testing_size = int(test_size*len(features))

    train_x = list(features[:,0][:-testing_size])
    train_y = list(features[:,1][:-testing_size])
    test_x = list(features[:,0][-testing_size:])
    test_y = list(features[:,1][-testing_size:])
    
    return train_x, train_y, test_x, test_y

if __name__ == '__main__':
    train_x, train_y, test_x, test_y = create_feature_sets_and_labels('weather_data.csv')
    with open('solarRadiation.pickle', 'wb') as f:
        pickle.dump([train_x, train_y, test_x, test_y], f)
