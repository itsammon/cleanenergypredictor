This is the elements needed for the Weather Prediction software for the flourish system.

The NN is contained in neuralNetwork.py. Adjustments to the NN are made here.

To train the system, first run "python dataCombiner.py". Then run "python neuralNetwork.py".
These files read in the data from apogee_rooftop and UCC_simple and combine them into weather_data.csv, and then turn those files into the neural network.

To use the system, call estimate_energy_output(year, day_of_year, hour, precipitation, sky_cond, temp) from energyConverter.py.

Graphs of days and the expectedVSActual outputs are provided.
