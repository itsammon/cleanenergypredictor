from __future__ import division
import math
from dataCombiner import to_sin_cos, getElevation, sunPosition, get_radiation_normalizer
from neuralNetwork import run_neural_network
from dateutil.parser import parse
from datetime import datetime, timedelta
import numpy as np
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

hm_lines = 10

# pvArea is in m^2 of area for the cells themselves
pvArea = 1

pvEfficiency = .20

# pvAngle is angle of solar panels from the horizontal in degrees
pvAngle = 15

#Angle is gotton from a nasa site
def get_energy(date, day_of_year, hour, precipitation, sky_cond, temp):
    sincos_doy = to_sin_cos(day_of_year, 366)
    sincos_hour = to_sin_cos(hour, 24)

    # get the solar elevation
    elev = getElevation(date)

    if elev < 0:
        sunUp = 0
    else:
        sunUp = 1

    sun = sunPosition(date)
    if sunUp == 0:
        return 0.0
    
    inputArray = [sincos_doy[0], sincos_doy[1], sincos_hour[0], sincos_hour[1], sunUp, sun[0], sun[1], precipitation, sky_cond, temp]
    result = run_neural_network(inputArray)
    #if result < 0:
    #    result = 0.0
    return result*get_radiation_normalizer();
    

# Use this function to find the expected energy output
def estimate_output_energy(year, day_of_year, hour, precipitation, sky_cond, temp):
    
    date = str(year) + '-' + str(day_of_year) + '-' + str(hour) + ' MST'
    date1 = datetime.strptime(date, '%Y-%j-%H %Z')
    sun = sunPosition(date1)

    energy = get_energy(date1, day_of_year, hour, precipitation, sky_cond, temp)
    print energy
    # Uses formula eff*E*A*cos(angle)  where angle is the average zenith angle - solar panel angle
    # The effective area of a solar panel is Acos(theta) where theta is the deviation from 90 degrees
    output = pvArea * pvEfficiency * energy * (math.cos(math.radians(pvAngle) - sun[1]))
    return output

def day_from_sin_cos(sine, cosine):
    print asin(sine)

def print_estimated_energy_graph():
    offset = 10
    row_count = 1
    items = []
    predictedEnergy = []
    actualEnergy = []
    rad_norm = get_radiation_normalizer()
    with open('weather_data.csv', 'r') as f:
        contents = f.readlines()
        for l in contents[offset+1:hm_lines+offset+1]:
            items = l.strip().split(",")

            inputArray = items[:-1]
            
            result = run_neural_network(inputArray)

            print result

            predictedEnergy.append(result)
            actualEnergy.append(items[-1])
                
            row_count += 1

    for x in xrange(hm_lines):
        predictedEnergy[x] = predictedEnergy[x]*rad_norm
    for y in xrange(hm_lines):
        actualEnergy[y] = float(actualEnergy[y])*rad_norm
    
    pred = plt.plot([x+8 for x in xrange(hm_lines)], predictedEnergy, linewidth=1.0, color='b', label='Predicted')
    actual = plt.plot([x+8 for x in xrange(hm_lines)], actualEnergy, linewidth=1.0, color='r', label='Actual')
    pred_patch = mpatches.Patch(color='blue', label='Predicted')
    actual_patch = mpatches.Patch(color='red', label='Actual')
    
    plt.legend(handles=[pred_patch, actual_patch])
    plt.title('Example of Predicted and Actual Solar Radiation')
    plt.xlabel('Hour of Day')
    plt.ylabel('Solar Radiation (W/m^2)')
    plt.show()

def print_estimated_energy_graph_integral():
    row_count = 0
    num_days = 1
    curr_sin = 0
    curr_cos = 0
    prev_pred_value = -1000
    prev_actual_value = -1000
    pred_integral = 0
    actual_integral = 0
    items = []
    actual = 0
    predictedEnergy = []
    actualEnergy = []
    rad_norm = get_radiation_normalizer()
    for line in open('weather_data.csv', 'r'):
        # Skip the first line
        if not row_count == 0:
            items = line.strip().split(",")

            sine = float(items[0])
            cosine = float(items[1])

            print 'Getting prediction'
            result = run_neural_network(items[:-1])
            result = result*rad_norm
            actual = float(items[-1])*rad_norm

            if not sine == curr_sin:
                print 'New Day'
                print (items[0], items[1])
                num_days += 1
                predictedEnergy.append(pred_integral)
                actualEnergy.append(actual_integral)
                curr_sin = sine
                curr_cos = cosine
                pred_integral = 0
                actual_integral = 0
                prev_pred_value = result
                prev_actual_value = actual
            else:
                print 'Reading next value'
                pred_integral += (result+prev_pred_value)/2
                actual_integral += (actual+prev_actual_value)/2
                prev_pred_value = result
                prev_actual_value = actual
                
                
            row_count += 1
        else:
            row_count += 1

        # Get the last days information
        num_days += 1
        predictedEnergy.append(pred_integral)
        actualEnergy.append(actual_integral)

    plt.scatter(predictedEnergy, actualEnergy)
    plt.title('Predicted vs Actual')
    plt.xlabel('Integral of Predicted Energy on day (W/m^2)')
    plt.ylabel('Integral of Actual Energy on day (W/m^2)')
    plt.show()

if __name__ == '__main__':
    #print_estimated_energy_graph()
    #print_estimated_energy_graph_integral()
    print 'Year: 2012\nDay of Year: 165\nHour: 12\nPrecipitation: 0\nSky Condition: 0\nTemperature: 95 F\n'
    estimate_output_energy(2012, 165, 12, 0, 0, 95)
