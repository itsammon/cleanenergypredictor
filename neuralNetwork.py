from __future__ import division
from dataProcessing import create_feature_sets_and_labels
import tensorflow as tf
#from tensorflow.examples.tutorials.mnist import input_data
#mnist = input_data.read_data_sets("/tmp/data/", one_hot = True)
import pickle
import numpy as np
import matplotlib.pyplot as plt

# Create system to handle data
train_x, train_y, test_x, test_y = create_feature_sets_and_labels('weather_data.csv')

n_nodes_hl1 = 5
n_nodes_hl2 = 5
n_nodes_hl3 = 1
data_size = len(train_x[0])
n_classes = 1
batch_size = 1000

hm_epochs = 1000

x = tf.placeholder('float', [None, data_size])
y = tf.placeholder('float')


hidden_1_layer = { 'f_fum':n_nodes_hl1,
                       'weights': tf.Variable(tf.random_normal([data_size, n_nodes_hl1], stddev=0.3)),
                       'biases': tf.Variable(tf.random_normal([n_nodes_hl1], stddev=0.3))}
hidden_2_layer = { 'f_fum': n_nodes_hl2,
                       'weights': tf.Variable(tf.random_normal([n_nodes_hl1, n_nodes_hl2])),
                       'biases': tf.Variable(tf.random_normal([n_nodes_hl2]))}
#hidden_3_layer = { 'f_fum':n_nodes_hl3,
#                       'weights': tf.Variable(tf.random_normal([n_nodes_hl2, n_nodes_hl3])),
#                       'biases': tf.Variable(tf.random_normal([n_nodes_hl3]))}
output_layer = { 'f_fum':None,
                     'weights': tf.Variable(tf.random_normal([n_nodes_hl1, n_classes], stddev=0.3)),
                     'biases': tf.Variable(tf.random_normal([n_classes], stddev=0.3))}

def neural_network_model(data):

    l1 = tf.add(tf.matmul(data,hidden_1_layer['weights']), hidden_1_layer['biases'])
    l1 = tf.nn.relu(l1)

    l2 = tf.add(tf.matmul(l1, hidden_2_layer['weights']), hidden_2_layer['biases'])
    l2 = tf.nn.relu(l2)

    #l3 = tf.add(tf.matmul(l2, hidden_3_layer['weights']), hidden_3_layer['biases'])
    #l3 = tf.nn.relu(l3)

    output = tf.matmul(l2, output_layer['weights']) + output_layer['biases']

    return output

init = tf.global_variables_initializer()
saver = tf.train.Saver()

def train_neural_network(x):
    prediction = neural_network_model(x)
    cost = tf.reduce_mean(tf.square(y - prediction))
    optimizer = tf.train.AdamOptimizer(learning_rate=0.004, epsilon=0.1).minimize(cost)
    epoch = 1
    costs = []
    
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        try:
            epoch = int(open(tf_log,'r').read().split('\n')[-2])+1
            print('STARTING:', epoch)
        except:
            epoch = 1

        while epoch <= hm_epochs:
            if epoch != 1:
                ckpt = tf.train.get_checkpoint_state('./')
                if ckpt and ckpt.model_checkpoint_path:
                    saver.restore(sess, ckpt.model_checkpoint_path)
            epoch_loss = 1
            i = 0
            while i < len(train_x):
                start = i
                end = i+batch_size
                batch_x = np.array(train_x[start:end])
                batch_y = np.array(train_y[start:end])
                batches_run = 0
                _, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
                epoch_loss += c
                costs.append(c)
                i += batch_size

            print('Epoch %d completed out of %d, mean_error_squared: %f' % (epoch, hm_epochs, epoch_loss))
            save_path = saver.save(sess, "./model.ckpt")
            epoch += 1

        error = tf.square(y - prediction)

        accuracy = tf.reduce_mean(tf.cast(error, 'float'))
        print('Error:', accuracy.eval({x:test_x, y:test_y}))

        plt.plot(costs)
        plt.xlabel('Epochs')
        plt.ylabel('Mean Squared Error(training data)')
        plt.title('Error in prediction for training data')
        plt.show()

def run_neural_network(X):
    # Setup our data to work with
    test = np.array(X)
    test = test.astype(np.float)
    test = np.reshape(test, (1, data_size))
    prediction = neural_network_model(x)
    with tf.Session() as session:
        ckpt = tf.train.get_checkpoint_state('./')
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(session, ckpt.model_checkpoint_path)
        result = session.run(prediction, feed_dict={x: test})
    #print result
    return result[0][0]

if __name__ == '__main__':
    train_neural_network(x)
                                                                
