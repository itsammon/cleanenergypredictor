import numpy as np
import random
import pickle
import math
from dateutil.parser import parse
from collections import Counter
from datetime import datetime, timedelta
from pytz import timezone
import pytz
import astral

hm_lines = 1000000
radiation_normalizer = 5000

# Used to find the non numerical data
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def to_sin_cos(value, period):
    sin_cos = (math.sin((2*math.pi)/period*value), math.cos(2*math.pi*value/period))
    return sin_cos

def get_radiation_normalizer():
    return radiation_normalizer


def dayTimeToAzimuth(dayTime):
    return math.radians(city.solar_azimuth(dayTime))

# Find the angle of the sun at given times
utc = pytz.UTC

a = astral.Astral()
a.solar_depression = 'civil'
city = astral.Location(('Logan, Utah', 'USA', 41.7370, -111.8338, 'US/Mountain', 1382))
#print city


def getElevation(dayTime):
    return city.solar_elevation(dayTime)

def sunPosition(dayTime):
    #returns position of the sun in the sky for this day and time
    elevationAngle = max(math.radians(getElevation(dayTime)),math.radians(20))
    zenithAngle = math.pi/2 - elevationAngle
    return (elevationAngle, zenithAngle)


#def sunTime(dayTime,timeDelay=0):
# #returns the ADJUSTED sunrise and sunset dayTimes for this day
# #calculations ignore the timedelay of sunrise/set
# sun = city.sun(date=dayTime, local = True)
# sunRise = sun['sunrise'].replace(second=0, microsecond=0) + relativedelta(minutes=+timeDelay)
# sunSet = sun['sunset'].replace(second=0, microsecond=0) + relativedelta(minutes=-timeDelay)
# return (sunRise, sunSet)
def sunTime(dayTime,timeDelay=0):
    #returns the ADJUSTED sunrise and sunset dayTimes for this day
    #calculations ignore the timedelay of sunrise/set
    try:
        sun = city.sun(date=dayTime, local = True)
        sunRise = sun['sunrise'].replace(second=0, microsecond=0) + relativedelta(minutes=+timeDelay)
        sunSet = sun['sunset'].replace(second=0, microsecond=0) + relativedelta(minutes=-timeDelay)
        return (sunRise, sunSet)
    except Exception:
        sunSet = dayTime.replace(hour=23, minute=59,second=0, microsecond=0)
        sunSet = sunSet + relativedelta(minutes=+1)
        sunRise = dayTime.replace(hour=0, minute=0,second=0, microsecond=0)
        return (sunRise, sunSet)



def read_solar_radiation_data(data, outfile):
    row_count = 1
    items = []
    hour = 0
    radiation_avg = 0.0
    results = []
    nextResult = 0
    maxRadiation = -10000.0

    with open(data, 'r') as radiation, open('UCC_simple.csv', 'r') as ucc_file, open(outfile, 'w') as out:
        # Create a label for the output file
        result = 'DOY sin comp,DOY cos comp,hour sin comp,hour cos comp,sun up,solar elevation,solar azimuth, Precipitation,Sky Condition,Temperature,AverageRadiation/' + str(radiation_normalizer) +'\n'
        out.write(str(result))

        # read in first file
        contents = radiation.readlines()
        for l in contents[:hm_lines]:
            items = l.strip().split(",")
            #print items
            if row_count < 4:
                pass
            else:
                for i in xrange(5):
                    items[i+3] = float(items[i+3])
                for i in xrange(4):
                    if items[i+4] < 0:
                        items[i+4] = 0;
                if row_count % 4 == 0:
                    radiation_avg = float(sum(items[4:8]))/max(len(items[4:8]), 1)
                    hour = int(math.floor(float(items[2])))
                else:
                    radiation_avg += float(sum(items[4:8]))/max(len(items[4:8]), 1)
                #Combine rows for output
                if row_count % 4 == 3:
                    date = items[0] + '-' + items[1] + '-' + str(hour) + ' MST'
                    date1 = datetime.strptime(date, '%Y-%j-%H %Z')

                    # Convert to sin cos for data input
                    hour = to_sin_cos(date1.hour, 24)
                    yday = to_sin_cos(date1.timetuple().tm_yday, 366)
                
                    toCombine = next(read_large_file(ucc_file))
                    if toCombine[1] == 'T':
                        toCombine[1] = '0'

                    date2 = datetime.strptime(toCombine[0], '%Y-%m-%d %I:%M %p %Z')

                    if math.isnan(radiation_avg):
                        #print 'Radiation: ' + str(radiation_avg)
                        pass

                    if not is_number(toCombine[1]):
                        #print 'Precipitation:' + toCombine[1]
                        if toCombine[1] == 'T':
                            toCombine[1] = 0
                    if not is_number(toCombine[2]):
                        #print 'Sky condition: ' + toCombine[2]
                        pass
                    if not is_number(toCombine[3]):
                        #print 'Temperature: ' + toCombine[3]
                        pass

                    if date1 == date2 and not math.isnan(radiation_avg) and is_number(toCombine[1]) and is_number(toCombine[2]) and is_number(toCombine[3]):
                        elev = getElevation(date1)
                        
                        sun = sunPosition(date1)
                        #print sun

                        if elev < 0:
                            sunUp = 0
                        else:
                            sunUp = 1

                        
                        if radiation_avg > maxRadiation:
                            maxRadiation = radiation_avg
                        result = str(yday[0]) + ','+ str(yday[1]) + ',' + str(hour[0]) + ',' + str(hour[1]) + ',' + str(sunUp) + ',' + str(sun[0]) + ',' + str(sun[1]) + ','
                        result += toCombine[1] + ',' + toCombine[2] + ',' + toCombine[3] + ',' + str(radiation_avg/10000) + '\n'
                        #print result

                        if sunUp == 1:
                            out.write(str(result))

                        #results.append(result)

                        nextResult += 1

            row_count += 1
    print maxRadiation

def read_large_file(file_object):
    while True:
        data = file_object.readline()
        data = data.strip().split(",")
        yield data


if __name__ == '__main__':
    read_solar_radiation_data('solar_radiation_data_from_apogee_rooftop.csv', 'weather_data.csv')
